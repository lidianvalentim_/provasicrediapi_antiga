#language: pt
Funcionalidade: Consultar CEP e verificar o retorno

  Como usuário gostaria de submeter GET como request para o webservice ViaCep

  @cepvalido
  Esquema do Cenário: : Consultar CEP valido
    Dado que o usuário insere um "<CEP>" válido
    Quando o serviço é consultado
    Então é retornado o CEP, "<logradouro>", "<complemento>", "<bairro>", "<localidade>", "<UF>" e "<IBGE>".
    Exemplos:
      | CEP      | logradouro      | complemento | bairro   | localidade | UF | IBGE    |
      | 50670220 | Rua Rio Formoso |             | Iputinga | Recife     | PE | 2611606 |

  @cepinexistente
  Esquema do Cenário: Cenário: Consultar CEP inexistente
    Dado que o usuário insere um "<CEP>" que não exista na base dos Correios
    Quando o serviço é consultado
    Então é retornada um atributo erro
    Exemplos:
      | CEP      |
      | 00000000 |

  @cepinvalido
  Esquema do Cenário: Cenário: Consultar CEP com formato inválido
    Dado que o usuário insere um "<CEP>" com formato inválido
    Quando o serviço é consultado
    Então é retornado uma mensagem de erro
    Exemplos:
      | CEP   |
      | 50670 |