#language: pt
Funcionalidade: Consultar Endereço e pesquisar CEP

  Como usuário gostaria de verificar o retorno do webservice ViaCep

  @pesquisarcep
  Cenário: Cenário: Pesquisar e verificar o retorno do webservice
    Dado que o usuário insere um endereço válido
    Quando o serviço é consultado
    Então é retornado as informações do endereço