package org.restassuredsicred.stepDefinition;

import io.cucumber.java.BeforeStep;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;

public class Hooks {
    @BeforeStep
    public static void beforeClass() {
        enableLoggingOfRequestAndResponseIfValidationFails();
        baseURI = "https://viacep.com.br/";
        basePath = "/ws";

        requestSpecification = new RequestSpecBuilder().
                setContentType(ContentType.JSON).
                build();

        responseSpecification = new ResponseSpecBuilder().
                expectContentType(ContentType.JSON).
                build();
    }
}