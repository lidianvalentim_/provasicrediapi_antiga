package org.restassuredsicred.stepDefinition;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;


public class ConsultarSteps {

    @Dado("que o usuário insere um {string} válido")
    public void que_o_usuário_insere_um_cep_válido(String cep) throws Exception {
        given().
                param("/json").
                get("/" + cep + "/json");
    }

    @Quando("o serviço é consultado")
    public void o_serviço_é_consultado() throws Exception {
        given().
                then().
                statusCode(HttpStatus.SC_OK);
    }

    @Então("é retornado o CEP, {string}, {string}, {string}, {string}, {string} e {string}.")
    public void é_retornado_o_cep_logradouro_complemento_bairro_localidade_uf_e_ibge(String logradouro, String complemento, String bairro, String localidade, String UF, String IBGE) throws Exception {
        given().
                body("cep").
                then().
                statusCode(HttpStatus.SC_OK).
                body(
                        "logradouro", is(logradouro),
                        "complemento", is(complemento),
                        "bairro", is(bairro),
                        "localidade", is(localidade),
                        "uf", is(UF),
                        "ibge", is(IBGE)
                );
    }

    @Dado("que o usuário insere um {string} que não exista na base dos Correios")
    public void que_o_usuário_insere_um_cep_que_não_exista_na_base_dos_correios(String cep) throws Exception {
        given().
                param("/json").
                when().
                get("/" + cep + "/json").
                then().
                statusCode(HttpStatus.SC_OK);
    }

    @Então("é retornada um atributo erro")
    public void é_retornada_um_atributo_erro() throws Exception {
        given().
                param("/json").
                then().
                body("erro", is("true"));

    }

    @Dado("que o usuário insere um {string} com formato inválido")
    public void que_o_usuário_insere_um_cep_com_formato_inválido(String cep) throws Exception {
        given().
                param("/json").
                when().
                get("/" + cep + "/json");
    }

    @Então("é retornado uma mensagem de erro")
    public void éRetornadoUmaMensagemDeErro() throws Exception {
        given().
                param("/json").
                then().
                statusCode(HttpStatus.SC_BAD_REQUEST);
    }
}
