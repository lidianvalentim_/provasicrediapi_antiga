package org.restassuredsicred.stepDefinition;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import org.apache.http.HttpStatus;
import org.restassuredsicred.Domain.User;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class PesquisarSteps {

    @Dado("que o usuário insere um endereço válido")
    public void queOUsuárioInsereUmEndereçoVálido() throws Exception {
        given().
                when().
                get("/RS/Gravatai/Barroso/json").
                then().
                statusCode(HttpStatus.SC_OK);
    }

    @Então("é retornado as informações do endereço")
    public void éRetornadoAsInformaçõesDoEndereço() throws Exception {
      // User user = given(). não precisa guardar em usuario
        given().
                when().
                get("/RS/Gravatai/Barroso/json").
                then().
                statusCode(HttpStatus.SC_OK).
                body("cep", Matchers.hasItems("94085-170", "94175-000")),
                body("Logradouro", Matchers.hasItems("Rua Ari Barroso", "Rua Almirante Barroso")),
                body("Compelento", Matchers.hasItems("", "")),
                body("Bairro", Matchers.hasItems("Morada do Vale I", "Recanto Corcunda")),
                body("Localidade", Matchers.hasItems("Gravataí", "Gravataí")),
                body("Uf", Matchers.hasItems("RS", "RS")),
                body("Ibge", Matchers.hasItems("4309209", "4309209"));

    }
}
